# Open Tracking Android

Open Tracking is an open-source mobile tracking platform (currently Android-only). The platform consists of 3 main projects:

#### Open Tracking API

[https://bitbucket.org/bef126/open-tracking-api](https://bitbucket.org/bef126/open-tracking-api)

The Open Tracking API contains the API that the Android project uses and the tracking website for displaying the phone's location on a map. It is written in [Google Go](https://golang.org) to be run on [Google App Engine](https://cloud.google.com/appengine/docs).

#### Open Tracking Websocket

[https://bitbucket.org/bef126/open-tracking-websocket](https://bitbucket.org/bef126/open-tracking-websocket)

The Open Tracking Websocket server is a simple [socket.io](http://socket.io) server. It facilitates the real-time location updates between the mobile device and the tracking website. It was designed to run on a [Heroku](http://heroku.com) dyno, but can be easily modified to run on any [Node.js](http://nodejs.org) container.

#### Open Tracking Android

[https://bitbucket.org/bef126/open-tracking-android](https://bitbucket.org/bef126/open-tracking-android)

The Open Tracking Android project contains the mobile tracking app. The tracking app allows you to start and stop tracking, as well as send email invites for people to view the tracked location on a map.

## Setup

This project was created with [Android Studio](http://developer.android.com/tools/studio/index.html), so the project import wizards should work. If you want to deploy this yourself, simply change the package name, then modify the `HOST` parameter in `Constants.java` to point to your own API server.

## Google Play Store

This app is currently deployed at the following URL:

[https://play.google.com/store/apps/details?id=com.appspot.opentracking](https://play.google.com/store/apps/details?id=com.appspot.opentracking)

