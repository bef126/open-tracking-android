package com.appspot.opentracking;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, CompoundButton.OnCheckedChangeListener {

    private static final String TAG = "MainActivity";
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private Intent serviceIntent;

    private BroadcastReceiver totalSecondsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            long seconds = intent.getLongExtra("SECONDS", 0);
            displaySeconds(seconds);
        }
    };

    private BroadcastReceiver totalMilesReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            double miles = intent.getDoubleExtra("MILES", 0.0);
            displayMiles(miles);
        }
    };

    private BroadcastReceiver forceServiceShutdownReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            forceServiceShutdown();
        }
    };

    private TextView tvSeconds;
    private TextView tvMiles;
    private Button bSendInvite;
    private Switch sSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        // pre-select the first item
        mNavigationDrawerFragment.selectItem(0);

        bSendInvite = (Button) findViewById(R.id.sendInvite);

        sSwitch = (Switch) findViewById(R.id.switch1);
        if (sSwitch != null) {
            sSwitch.setOnCheckedChangeListener(this);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(totalSecondsReceiver, new IntentFilter("total-seconds-pulse"));
        LocalBroadcastManager.getInstance(this).registerReceiver(totalMilesReceiver, new IntentFilter("total-miles-pulse"));
        LocalBroadcastManager.getInstance(this).registerReceiver(forceServiceShutdownReceiver, new IntentFilter("force-service-shutdown"));
        updateButtonState();
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(totalSecondsReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(totalMilesReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(forceServiceShutdownReceiver);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        Log.d("position", "" + position);
        switch (position) {
            case 0:
                break;
            case 1:
                showSettings();
                mNavigationDrawerFragment.selectItem(0);
                break;
            case 2:
                logout();
                break;
            case 3:
                about();
                mNavigationDrawerFragment.selectItem(0);
                break;
        }
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.global, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            showSettings();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void displaySeconds(long seconds) {
        Log.d(TAG, "seconds: " + seconds);
        try {
            if (tvSeconds == null) {
                tvSeconds = (TextView) findViewById(R.id.seconds);
            }

            tvSeconds.setText("" + formatSeconds(seconds));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String formatSeconds(long seconds) {
        int hours = (int) seconds / 3600;
        int remainder = (int) seconds - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;

        return String.format("%02d", hours) + ":" + String.format("%02d", mins) + ":" + String.format("%02d", secs);
    }

    private void displayMiles(double miles) {
        Log.d(TAG, "miles: " + miles);
        try {
            if (tvMiles == null) {
                tvMiles = (TextView) findViewById(R.id.miles);
            }
            NumberFormat formatter = new DecimalFormat("#0.00");
            tvMiles.setText("" + formatter.format(miles) + " miles");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startService() {
        resetDisplay();
        stopService();
        serviceIntent = new Intent(this, TrackingService.class);
        startService(serviceIntent);
        bSendInvite.setVisibility(View.VISIBLE);
    }

    private void stopService() {
        if (serviceIntent != null) {
            try {
                stopService(serviceIntent);
                bSendInvite.setVisibility(View.INVISIBLE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void resetDisplay() {
        displayMiles(0.0);
        displaySeconds(0);
    }

    public void sendInvite(View view) {
        final Context me = this;
        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {
            ProgressDialog progressDialog;

            @Override
            protected void onPreExecute() {
                progressDialog = ProgressDialog.show(me, "", getString(R.string.please_wait), false);
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... p) {
                Api api = new Api(me);
                try {
                    return api.createInvite();
                } catch (Exception e) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
                if (result == null) {
                    showMessage(getString(R.string.send_invite_error));
                }
                try {
                    // join channel
                    broadcastJoinChannel(result);

                    final Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/html");
                    intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.invite_subject));
                    intent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml("<p>The following invite is good for the next 24 hours:</p><p><a href='" + Constants.HOST + "/track?t=" + result + "'>" + Constants.HOST + "/track?t=" + result + "</a></p>"));
                    progressDialog.dismiss();
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    progressDialog.dismiss();
                }
            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute();
    }

    private void updateButtonState() {
        if (isTrackingServiceRunning()) {
            bSendInvite.setVisibility(View.VISIBLE);
            sSwitch.setChecked(true);
        } else {
            bSendInvite.setVisibility(View.INVISIBLE);
            sSwitch.setChecked(false);
        }
    }

    public boolean isTrackingServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (TrackingService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void logout() {
        stopService();
        StorageUtil.removePref(Constants.EMAIL_LABEL, this);
        finish();
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
    }

    private void showSettings() {
        Intent i = new Intent(this, SettingsActivity.class);
        startActivity(i);
    }

    private void showMessage(String msg) {
        showMessage(null, msg);
    }

    private void showMessage(String title, String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(msg)
                .setTitle(title)
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog d = builder.create();
        d.show();
    }

    private void forceServiceShutdown() {
        bSendInvite.setVisibility(View.INVISIBLE);
        sSwitch.setChecked(false);
        showMessage(getString(R.string.idle_message));
    }

    private void about() {
        String version = getString(R.string.no_version);
        try {
            version = "version " + getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }

        showMessage(getString(R.string.app_name), version);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Switch s = (Switch) buttonView;
        Log.d(TAG, s.getText().toString());
        if (s.getText().toString().equals(getString(R.string.switch_text))) {
            if (isChecked) {
                startService();
            } else {
                stopService();
            }
        }
    }

    private void broadcastJoinChannel(String channel){
        Intent intent = new Intent("join-channel");
        intent.putExtra("CHANNEL", channel);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
