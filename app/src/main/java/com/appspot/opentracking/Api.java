package com.appspot.opentracking;

import android.content.Context;

import org.json.JSONObject;

public class Api {
    private HttpUtil http;
    private Context ctx;

    public Api(Context c) {
        this.http = new HttpUtil(c);
        this.ctx = c;
    }

    public JSONObject login(String email, String password) throws Exception {
        MyHttpParams params = new MyHttpParams();
        params.add("email", email);
        params.add("password", password);
        JSONObject j = http.doPost("/v1/login", params);
        StorageUtil.savePrefs(Constants.SESSION_TOKEN_LABEL, j.getString("Token"), ctx);
        StorageUtil.savePrefs(Constants.EMAIL_LABEL, email, ctx);
        StorageUtil.savePrefs(Constants.WEBSOCKET_SERVER_LABEL, j.getString("WebsocketServer"), ctx);
        StorageUtil.savePrefs(Constants.USER_ID_LABEL, j.getString("UserId"), ctx);

        return j;
    }

    public String createInvite() throws Exception {
        MyHttpParams params = new MyHttpParams();
        params.add("t", StorageUtil.getStringPref(Constants.SESSION_TOKEN_LABEL, ctx));
        JSONObject j = http.doPost("/v1/createInvite", params);
        return j.getString("Token");
    }

    public JSONObject register(String email, String password) throws Exception {
        MyHttpParams params = new MyHttpParams();
        params.add("email", email);
        params.add("password", password);
        http.doPost("/v1/register", params);

        // log new user in
        return login(email, password);
    }
}
