package com.appspot.opentracking;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import java.util.List;

public class LocationUtil {
    private LocationManager locationManager;
    private LocationListener locationListenerGPS;
    private LocationListener locationListenerNETWORK;
    private Location currentGPSLocation;
    private Location currentNETWORKLocation;
    private Location bestLocation;
    private int pollingIntervalInSeconds;
    private static final String TAG = "LocationUtil";

    public LocationUtil(int pollingIntervalInSeconds, Context c) {
        locationManager = (LocationManager) c.getSystemService(Context.LOCATION_SERVICE);
        this.pollingIntervalInSeconds = pollingIntervalInSeconds;
        initLocationServices();
    }

    private boolean providerExists(String name) {
        List<String> all = locationManager.getAllProviders();
        return all.contains(name);
    }

    private void initLocationServices() {
        locationListenerGPS = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                currentGPSLocation = location;
                setBestLocation();
            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {
            }

            @Override
            public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
            }
        };

        locationListenerNETWORK = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                currentNETWORKLocation = location;
                setBestLocation();
            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {
            }

            @Override
            public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
            }
        };
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, (pollingIntervalInSeconds * 1000), 0, locationListenerGPS);
        if (providerExists(LocationManager.NETWORK_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, (pollingIntervalInSeconds * 1000), 0, locationListenerNETWORK);
            currentNETWORKLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
        currentGPSLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        setBestLocation();
    }

    private boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        if (location.getAccuracy() > 100.0) {
            // we don't want anything less accurate then 100 meters
            return false;
        }

        return true;
    }

    private synchronized void setBestLocation() {
        if (currentGPSLocation != null && currentNETWORKLocation != null) {
            Log.d(TAG, "GPS Accuracy: " + currentGPSLocation.getAccuracy() + " - Network Accuracy: " + currentNETWORKLocation.getAccuracy());
            if (currentGPSLocation.getAccuracy() <= currentNETWORKLocation.getAccuracy()) {
                if (isBetterLocation(currentGPSLocation, bestLocation)) {
                    bestLocation = currentGPSLocation;
                }
            } else {
                if (isBetterLocation(currentNETWORKLocation, bestLocation)) {
                    bestLocation = currentNETWORKLocation;
                }
            }
        } else if (currentGPSLocation != null) {
            if (isBetterLocation(currentGPSLocation, bestLocation)) {
                bestLocation = currentGPSLocation;
            }
        } else if (currentNETWORKLocation != null) {
            if (isBetterLocation(currentNETWORKLocation, bestLocation)) {
                bestLocation = currentNETWORKLocation;
            }
        }
    }

    public Location currentBestLocation() {
        return bestLocation;
    }

    public void shutdown() {
        try {
            if (providerExists(LocationManager.NETWORK_PROVIDER)) {
                locationManager.removeUpdates(locationListenerNETWORK);
            }
            locationManager.removeUpdates(locationListenerGPS);
        } catch (Exception e) {
            Log.d(TAG, "could not remove location listeners");
        }
    }
}
