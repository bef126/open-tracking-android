package com.appspot.opentracking;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class WebsocketManager {
    private static final String TAG = "WebsocketManager";
    private String URL;
    private Socket socket;
    private Timer socketHealthChecktimer;
    private boolean socketConnected = false;
    private TrackingService ts;

    public WebsocketManager(String url, TrackingService t) {
        this.URL = url;
        this.ts = t;
    }

    private boolean connect() {
        try {
            socket = IO.socket(this.URL);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return false;
        }

        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                Log.v(TAG, "connected");
                socketConnected = true;
                startSocketHealthTimer();
            }

        }).on("ping", new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                ts.broadcastMyLocation();
            }

        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

            @Override
            public void call(Object... args) {
                Log.v(TAG, "disconnected");
                socketConnected = false;
            }

        });
        socket.connect();
        return true;
    }

    public void run() {
        connect();
    }

    public void stop() {
        stopSocketHealthTimer();
        socket.disconnect();
    }

    private void startSocketHealthTimer() {
        stopSocketHealthTimer();
        socketHealthChecktimer = new Timer();
        socketHealthChecktimer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                checkSocketHealth();
            }
        }, 0, (1000 * 10));
    }

    private void stopSocketHealthTimer() {
        try {
            socketHealthChecktimer.cancel();
        } catch (Exception e) {
            // timer wasn't running or was null
        }
    }

    public void broadcast(String channel, String event, Map data) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("channel", channel);
            obj.put("event", event);
            Iterator entries = data.entrySet().iterator();
            while (entries.hasNext()) {
                Map.Entry e = (Map.Entry) entries.next();
                obj.put(e.getKey().toString(), e.getValue().toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        socket.emit("broadcast", obj);
    }

    private synchronized void checkSocketHealth() {
        try {
            if (socketConnected == false) {
                socket.connect();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void joinChannel(String channel){
        try{
            socket.emit("join_channel", channel);
            Log.d(TAG, "joined channel: " + channel);
        } catch(Exception e){
            e.printStackTrace();
        }
    }
}