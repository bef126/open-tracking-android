package com.appspot.opentracking;

public class Constants {
    public static final String HOST = "https://open-tracking.appspot.com";

    // constants for saved settings
    public static final String POLLING_INTERVAL_LABEL = "polling-interval";
    public static final String SESSION_TOKEN_LABEL = "session-token";
    public static final String EMAIL_LABEL = "email";
    public static final String WEBSOCKET_SERVER_LABEL = "websocket-server";
    public static final String USER_ID_LABEL = "user-id";

    // constants for error values
    public static final String USER_DOES_NOT_EXIST_ERROR = "User Does Not Exist";
}
