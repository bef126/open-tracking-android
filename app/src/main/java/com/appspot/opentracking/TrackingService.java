package com.appspot.opentracking;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class TrackingService extends Service {
    private static final String TAG = "TrackingService";
    private Timer totalTimeTrackedTimer;
    private Timer pingLocationTimer;
    private Timer stopTrackingTimer;
    private long startTime;
    private double totalMiles = 0.0;
    public WebsocketManager wm;
    private LocationUtil lu;
    private int pollingIntervalInSeconds;
    private Location previousPingedLocation;
    private double mileageAtLastIdleCheck = 0.0;

    private BroadcastReceiver joinChannelReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String channel = intent.getStringExtra("CHANNEL");
            wm.joinChannel(channel);
        }
    };

    /**
     * Class for clients to access.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with
     * IPC.
     */
    public class LocalBinder extends Binder {
        TrackingService getService() {
            return TrackingService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        String wsServer = StorageUtil.getStringPref(Constants.WEBSOCKET_SERVER_LABEL, this);
        wm = new WebsocketManager(wsServer, this);
        pollingIntervalInSeconds = StorageUtil.getIntegerPref(Constants.POLLING_INTERVAL_LABEL, this);
        lu = new LocationUtil(pollingIntervalInSeconds, this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("TrackingService", "Received start id " + startId + ": " + intent);

        startTime = System.currentTimeMillis();

        startTimers();

        wm.run();

        // listen for join-channel broadcasts
        LocalBroadcastManager.getInstance(this).registerReceiver(joinChannelReceiver, new IntentFilter("join-channel"));

        CharSequence text = getText(R.string.tracking_service_starting);
        Notification notification = new Notification(R.drawable.ic_launcher, text, System.currentTimeMillis());
        notification.flags = Notification.FLAG_ONGOING_EVENT | Notification.FLAG_NO_CLEAR;
        Intent toLaunch = new Intent(getApplicationContext(), MainActivity.class);
        toLaunch.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intentBack = PendingIntent.getActivity(getApplicationContext(), 0, toLaunch, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setLatestEventInfo(this, getText(R.string.tracking_service_started_title), getText(R.string.tracking_service_started), intentBack);
        toLaunch.setAction("android.intent.action.MAIN");
        startForeground(startId, notification);
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        cleanup();
    }

    private void cleanup() {
        stopForeground(true);
        stopTimers();
        wm.stop();
        lu.shutdown();
        this.stopSelf();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(joinChannelReceiver);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    // This is the object that receives interactions from clients.  See
    // RemoteService for a more complete example.
    private final IBinder mBinder = new LocalBinder();

    private void startTimers() {
        stopTimers();

        try {
            totalTimeTrackedTimer = new Timer();
            totalTimeTrackedTimer.scheduleAtFixedRate(new TimerTask() {
                public void run() {
                    broadcastSeconds();
                    broadcastMiles();
                }
            }, 1000, 1000);
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        try {
            pingLocationTimer = new Timer();
            pingLocationTimer.scheduleAtFixedRate(new TimerTask() {
                public void run() {
                    pingLocation();
                }
            }, (pollingIntervalInSeconds * 1000), (pollingIntervalInSeconds * 1000));
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }

        try {
            long tenMinutes = 1000 * 60 * 10;
            stopTrackingTimer = new Timer();
            stopTrackingTimer.scheduleAtFixedRate(new TimerTask() {
                public void run() {
                    checkForStopTracking();
                }
            }, (tenMinutes), (tenMinutes));
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    private void stopTimers() {
        try {
            totalTimeTrackedTimer.cancel();
        } catch (Exception e) {
            // don't care
        }

        try {
            pingLocationTimer.cancel();
        } catch (Exception e) {
            // don't care
        }

        try {
            stopTrackingTimer.cancel();
        } catch (Exception e) {
            // don't care
        }
    }

    private void broadcastSeconds() {
        Intent intent = new Intent("total-seconds-pulse");
        intent.putExtra("SECONDS", elapsedSeconds());
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void broadcastMiles() {
        Intent intent = new Intent("total-miles-pulse");
        intent.putExtra("MILES", totalMiles);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private double haversine(double lat1, double lon1, double lat2, double lon2) {
        final double R = 3959;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return R * c;
    }

    // have we moved at least 50 feet since the last ping
    private boolean shouldPingLocation(Location l) {
        double miles = haversine(l.getLatitude(), l.getLongitude(), previousPingedLocation.getLatitude(), previousPingedLocation.getLongitude());
        double fiftyFeet = 0.0094697; // 50-feet in miles
        return miles >= fiftyFeet;
    }

    private synchronized void addMileage(Location l) {
        double miles = haversine(l.getLatitude(), l.getLongitude(), previousPingedLocation.getLatitude(), previousPingedLocation.getLongitude());
        totalMiles += miles;
    }

    private synchronized void pingLocation() {
        try {
            Location cbl = lu.currentBestLocation();

            // if previousPingedLocation hasn't been set, set that first
            if (previousPingedLocation == null) {
                previousPingedLocation = cbl;
                broadcastLocation(previousPingedLocation);
                return;
            }

            if (shouldPingLocation(cbl)) {
                broadcastLocation(cbl);
                addMileage(cbl);
                previousPingedLocation = cbl;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void broadcastLocation(Location l) {
        Log.d(TAG, "broadcastLocation: " + l.toString());
        String channel = StorageUtil.getStringPref(Constants.USER_ID_LABEL, this);
        Map<String, String> dictionary = new HashMap<String, String>();
        dictionary.put("lat", "" + l.getLatitude());
        dictionary.put("lng", "" + l.getLongitude());
        double mph = l.getSpeed() * 2.2369362920544;
        dictionary.put("spd", "" + mph);
        wm.broadcast(channel, "move", dictionary);
    }

    public void broadcastMyLocation(){
        try {
            Location cbl = lu.currentBestLocation();
            broadcastLocation(cbl);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    private long elapsedSeconds() {
        long endTime = System.currentTimeMillis();
        long tDelta = endTime - startTime;
        return (tDelta / 1000);
    }

    // if you fail to move 1/10th of a mile in 10 min, we'll automatically stop tracking
    private void checkForStopTracking() {
        double distanceTraveled = totalMiles - mileageAtLastIdleCheck;
        if (distanceTraveled < 0.1) {
            stopTracking();
        } else {
            mileageAtLastIdleCheck = totalMiles;
        }
    }

    private void stopTracking() {
        Intent intent = new Intent("force-service-shutdown");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        cleanup();
    }
}
