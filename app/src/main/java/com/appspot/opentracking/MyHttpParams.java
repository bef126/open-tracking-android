package com.appspot.opentracking;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyHttpParams {
    private static final String TAG = "MyHttpParams";
    private Map<String, String> params;

    public MyHttpParams() {
        params = new HashMap<String, String>();
    }

    public void add(String key, String value) {
        params.put(key, value);
    }

    public String toString() {
        String res = "";
        for (Map.Entry<String, String> entry : params.entrySet()) {
            try {
                res += URLEncoder.encode(entry.getKey()) + "=" + URLEncoder.encode(entry.getValue()) + "&";
            } catch (Exception e) {
                Log.e(TAG, "Error parsing parameter " + entry.getKey() + ". assuming null");
                res += URLEncoder.encode(entry.getKey()) + "=" + "&";
            }
        }
        if (res.endsWith("&")) {
            res = res.substring(0, res.length() - 1);
        }
        return res;
    }

    public UrlEncodedFormEntity forPost() {
        List<NameValuePair> results = new ArrayList<NameValuePair>();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            try {
                results.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            } catch (Exception e) {
                Log.d(TAG, "Error parsing parameter " + entry.getKey() + ". assuming null");
                results.add(new BasicNameValuePair(entry.getKey(), null));
            }
        }
        UrlEncodedFormEntity ret = null;
        try {
            ret = new UrlEncodedFormEntity(results);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }
}
